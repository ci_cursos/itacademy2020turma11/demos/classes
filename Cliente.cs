using System;

namespace classes
{
    public class Cliente
    {
        private string nome;
        private DateTime dataNascimento;
        private decimal limiteCredito;
        public Cliente(string umNome, string umaDataIso)
        {
            nome = umNome;
            dataNascimento = DateTime.Parse(umaDataIso);
        }
        public Cliente(string umNome)
        {
            nome = umNome;
            dataNascimento = DateTime.Now;
        }
        public void AumentarLimiteCredito(decimal porcentagem)
        {
            decimal aumento = limiteCredito * porcentagem;
            limiteCredito += aumento;
        }
        public decimal LimiteDeCredito
        {
            get
            {
                return limiteCredito;
            }
            set
            {
                if (value < 0)
                {
                    //Gerar uma exceção
                    throw new ArgumentOutOfRangeException($"{nameof(value)} deve ser maior ou igual a 0");
                }
                limiteCredito = value;
            }
        }
        public string Nome
        {
            get { return nome; }
        }
        public string DataDeNascimento
        {
            get { return dataNascimento.ToShortDateString();}
        }
        public int Idade
        {
            get
            {
                var anoAtual = DateTime.Now.Year;
                var anoNascimento = dataNascimento.Year;
                return anoAtual - anoNascimento;
            }
        }
        public string Cpf {get; private set;} = string.Empty;

    }
}