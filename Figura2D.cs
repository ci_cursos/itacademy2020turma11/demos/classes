using System;

namespace classes
{
    public abstract class Figura2D : Desenhavel
    {
        public int X {get;set;}
        public int Y {get;set;}
        public Figura2D(int posx, int posy)
        {
            X = posx;
            Y = posy;
        }
        public override string ToString()
        {
            return $"{base.ToString()}[X={X}, Y={Y}]";
        }
        public abstract double CalcularArea();
        public abstract void Desenha();
        private string cor;
        public string CorTraco {
            get => cor;
            set => cor = value;
        }
    }

    public class Quadrado : Figura2D
    {
        public int Lado {get;set;}

        public Quadrado(int posx, int posy, int tamanholado)
        : base(posx,posy)
        {
            Lado = tamanholado;
        }
        public override string ToString()
        {
            return $"{base.ToString()}[Lado={Lado}]";
        }

        public override double CalcularArea()
        {
            return Lado * Lado;
        }

        public override void Desenha()
        {
            Console.WriteLine($"Quadrado na tela:{this}");
        }
    }

    public class Circulo : Figura2D
    {
        public int Raio {get; set;}
        public Circulo(int tamanhoraio)
        :this(0,0,tamanhoraio)
        {
        }
        public Circulo(int posx, int posy, int tamanhoraio)
        :base(posx,posy)
        {
            Raio = tamanhoraio;
        }
        public new string ToString()
        {
            return $"{base.ToString()}[Raio={Raio}]";
        }
        public override double CalcularArea()
        {
            return Math.PI * Raio * Raio;
        }
        public override void Desenha()
        {
            Console.WriteLine($"Circulo na tela:{this}");
        }
    }

    public interface Desenhavel
    {
        void Desenha();
        string CorTraco {get; set;}
    }
}