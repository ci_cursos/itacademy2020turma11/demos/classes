﻿using System;
using System.Collections.Generic;
namespace classes
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Cliente c = new Cliente("John Doe", "10/12/1999");
            Console.WriteLine(c.LimiteDeCredito);
            c.LimiteDeCredito = 1000M;
            c.AumentarLimiteCredito(0.50M);
            Console.WriteLine($"Limite de crédito é {c.LimiteDeCredito:C}");
            if (c.Cpf == null)
            {
                Console.WriteLine("ops... null");
            }
            */
            Quadrado q1 = new Quadrado(1,1,5);
            //Console.WriteLine(q1);

            Figura2D f1;
            f1 = new Quadrado(1,1,1);
            Console.WriteLine(f1);
            Console.WriteLine(f1.CalcularArea());
            Figura2D f2;
            f2 = new Circulo(3);
            Console.WriteLine(((Circulo)f2).ToString());
            Console.WriteLine(f2.CalcularArea());

            Console.WriteLine(Teste.campo2);
            Teste t = new Teste("teste");
            Console.WriteLine(t.campo1);
            //t.campo1 = "outro";

            List<Desenhavel> figuras = new List<Desenhavel>();
            figuras.Add(new Quadrado(1,2,3));
            figuras.Add(new Quadrado(3,2,4));
            figuras.Add(new Circulo(5));
            foreach(var fig in figuras)
            {
                fig.Desenha();
            }
            figuras.ForEach(fig => fig.Desenha());
        }
    }
}
